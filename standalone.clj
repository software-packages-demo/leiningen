#!/usr/bin/env inlein

; http://inlein.org

'{:dependencies [[org.clojure/clojure "1.8.0"]
                 [cool-stuff/such-and-such "1.2.3"]]}

(require '[clojure.whatever :as whtv]
         '[cool-stuff.something :as sth])

(println "Hi! You passed:" *command-line-args*)